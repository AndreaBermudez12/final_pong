/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    //Vector2 ballPosition;
    //Vector2 ballSpeed;
    //int ballRadius;
    
    int playerLife = 100;
    int enemyLife = 100;
    
    //int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    int timeCounter = 99;
    
    int playerGoal = 0;             //Restaremos la vida de las barras mediante los goles
    int enemyGoal = 0;              //por ejemplo: 1 gol del player = enemyLife-10 
    
    //int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    bool fadeOut = true;        //booleano para que el logo haga fade out.
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    bool fadeIn = false;         //booleano para que el logo haga fade in.
    
    const int velocidady = 8;
    const int ballSize = 15;
    const int maxvelocity = 8;
    const int minvelocity = 8;
    
    //Booleano para hacer pausa
    bool pause = false;
    
    //Creamos booleano para hacer blink
    bool blink = false;
    
    //booleanos para las victorias
    bool playerVictory = false;
    bool enemyVictory = false;
   
    //Variables para hacer el logo
    
    Color recColor = ORANGE;
    Color tituloColor = YELLOW;
    //logoColor.a = ORANGE;
    
    //creamos variables para las palas
    //pala derecha
    Rectangle paladerecha;
   
    paladerecha.width = 15;
    paladerecha.height = 80;
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
   
    //Pala izquierda
    Rectangle palaizquierda;
   
    palaizquierda.width = 15;
    palaizquierda.height = 80;
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
    
    //Variables para la pelota
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    //Variables para la velocidad de la bola
    Vector2 ballvelocity;
    ballvelocity.x = minvelocity;
    ballvelocity.y = minvelocity;
    
    //Variables para las barras de vida
    Rectangle LifePlayerBack;
    
    LifePlayerBack.width = 250;
    LifePlayerBack.height = 30;
    LifePlayerBack.x = 100;
    LifePlayerBack.y = screenHeight/2 - 200 - LifePlayerBack.height/2;
    
    Rectangle LifePlayer;
    
    LifePlayer.width = 250;
    LifePlayer.height = 30;
    LifePlayer.x = 100;
    LifePlayer.y = screenHeight/2 - 200 - LifePlayerBack.height/2;
    
    Rectangle LifeEnemyBack;
    
    LifeEnemyBack.width = 275;
    LifeEnemyBack.height = 30;
    LifeEnemyBack.x = screenWidth - 75 - LifeEnemyBack.width;
    LifeEnemyBack.y = screenHeight/2 - 200 - LifeEnemyBack.height/2;
    
    Rectangle LifeEnemy;
    
    LifeEnemy.width = 275;
    LifeEnemy.height = 30;
    LifeEnemy.x = screenWidth - 75 - LifeEnemyBack.width;
    LifeEnemy.y = screenHeight/2 - 200 - LifeEnemyBack.height/2;
    
    
    //Creamos variable para poder hacer la IA
    int iaLinex = screenWidth/2;
       
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    //Texture2D Logo = LoadTexture("Resources/boladragon2.png");
    
    Texture2D PantallaGameplay = LoadTexture("Resources/gameplaypong.png");
    PantallaGameplay.height = 450;
    PantallaGameplay.width = 800;
    
    Texture2D PantallaTitulo = LoadTexture("Resources/dragonballpong.png");
    PantallaGameplay.height = 450;
    PantallaGameplay.width = 800;
    
    Texture2D Victoria = LoadTexture("Resources/victory.png");
    PantallaGameplay.height = 450;
    PantallaGameplay.width = 800;
    
    Texture2D Derrota = LoadTexture("Resources/loose.png");
    PantallaGameplay.height = 450;
    PantallaGameplay.width = 800;
    
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    InitAudioDevice();
    
    Sound fxGoal = LoadSound("Resources/Goal.wav");
    Sound fxRebote = LoadSound("Resources/rebote.wav");
    
    Music Gameplay = LoadMusicStream("Resources/Fight.ogg");
    
    PlayMusicStream(Gameplay);
    
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                framesCounter++;
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                //Creamos un bool para hacer un fade, ver en las variables.
             
                if (fadeOut)
                {
                    alpha += fadeSpeed;
                    
                    if(alpha >= 1.0f)
                        {
                            alpha = 1.0f;
                            fadeOut = !fadeOut;
                        }

                }else
                {
                    alpha -= fadeSpeed;
                    
                    if(alpha <= 0.0f)
                        {
                            alpha = 0.0f;
                            fadeOut = !fadeOut;
                        }
                }
                
                if(framesCounter > 210)
                {
                    fadeOut = false;
                }
                if (framesCounter >= 300)
                  {
                     screen = TITLE;
                     framesCounter = 0;
                  }
                  
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (!fadeOut)
                {
                    alpha += fadeSpeed;
                    
                    if(alpha >= 1.0f)
                        {
                            alpha = 1.0f;
                            fadeOut = !fadeOut;
                        }

                }
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
                    framesCounter++;
                    if (framesCounter % 30 == 0)
                        {
                            framesCounter = 0;
                            blink = !blink;
                        }
                        
                        if (IsKeyPressed (KEY_ENTER))
                        {
                            screen = GAMEPLAY;
                            framesCounter = 0;
                        }
            } break;
            
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                UpdateMusicStream(Gameplay);
                framesCounter++;
                /*
                if (framesCounter/60 >=99)
                {
                    secondsCounter--;
                    
                    if (secondsCounter == 0)
                    {
                        screen = ENDING;
                    }
                }
                */
                // TODO: Ball movement logic.........................(0.2p)
                
                //Gestionamos el movimiento de la bola estableciendo sus limites, cada vez que se marque gol,
                //la pelota reseteara su posicion
                
                if (ball.x > screenWidth - ballSize)
                {
                    PlaySound(fxGoal);
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballvelocity.x = minvelocity;
                    ballvelocity.y = minvelocity;
                    playerGoal = playerGoal + 1;
                    LifeEnemy.width = LifeEnemy.width - 27.5;
                    
                }else if (ball.x < ballSize)
                {
                    PlaySound(fxGoal);
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballvelocity.x = minvelocity;
                    ballvelocity.y = minvelocity;
                    enemyGoal = enemyGoal + 1;
                    LifePlayer.width = LifePlayer.width - 25;
                }
                
                if ((ball.y > screenHeight - ballSize) || (ball.y < ballSize))
                {
                    PlaySound(fxRebote);
                    ballvelocity.y *=-1;
                }
                
                // TODO: Player movement logic.......................(0.2p)
                
                //La pala que controlara el jugador sera la izquierda
                
                if (IsKeyDown (KEY_Q))
                {
                    palaizquierda.y -= velocidady;
                }
                
                if (IsKeyDown (KEY_A))
                {
                    palaizquierda.y += velocidady;
                }
                
                // TODO: Enemy movement logic (IA)...................(1p)
                //Para ello, vamos a establecer una linea en el medio (ver en variables).
                
                if (ball.x > iaLinex)
                {
                    if (ball.y > paladerecha.y){
                        paladerecha.y+=velocidady *0.6; 
                    }
                    
                    if (ball.y < paladerecha.y){
                        paladerecha.y-=velocidady *0.6;
                    }
                }
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if (CheckCollisionCircleRec (ball, ballSize, palaizquierda)){
                    if (ballvelocity.x<0){
                        if (abs (ballvelocity.x)<maxvelocity){
                            ballvelocity.x *=-1;
                            ballvelocity.y *=1;
                        }else{
                            ballvelocity.x *=1;
                        }
                    }
                }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if (CheckCollisionCircleRec (ball, ballSize, paladerecha)){
                    if (ballvelocity.x>0){
                        if (abs (ballvelocity.x)<maxvelocity){
                            ballvelocity.x *=-1;
                            ballvelocity.y *=1;
                        }else{
                            ballvelocity.x *=1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                //Esta desde la linia 181 a la 194.
                
                //Hay que establecer unos limites para que nuesta pala no se salga de pantalla, por arriba y por abajo.
                
                if(palaizquierda.y<0)
                {
                    palaizquierda.y = 0;
                }
       
                if(palaizquierda.y > (screenHeight - palaizquierda.height))
                {
                    palaizquierda.y = screenHeight - palaizquierda.height;
                }
                
                //Limites de la pala enemiga
                if(paladerecha.y<0)
                {
                    paladerecha.y = 0;
                }
       
                if(paladerecha.y > (screenHeight - paladerecha.height))
                {
                    paladerecha.y = screenHeight - paladerecha.height;
                }
                // TODO: Life bars decrease logic....................(1p)
                
                //esta escrito en las lineas 225 y 235

                // TODO: Time counter logic..........................(0.2p)


                // TODO: Game ending logic...........................(0.2p)
                
                //Si el jugador gana
                if (playerGoal == 10)
                {
                    screen = ENDING;
                }
                
                //Si el jugador pierde
                if (enemyGoal == 10)
                {
                    screen = ENDING;
                }
                
                //Si se agota el tiempo
                /*
                if (framesCounter/60 >=99)
                {
                    secondsCounter--;
                    scanf ("%i", &secondsCounter);
                    
                        if (secondsCounter == 0)
                            {
                                screen = ENDING;
                            }

                }
                */
                
                if (!pause)
                {
                    framesCounter++;
                    if (framesCounter > 60)
                    {
                        timeCounter--;
                        framesCounter = 0;
                    }
                    
                    if (timeCounter == 0)
                    {
                        screen = ENDING;
                    }
                }
                // TODO: Pause button logic..........................(0.2p)
                
                if (IsKeyPressed (KEY_P))
                {
                    pause = !pause;
                }
                
                if (!pause)
                {
                    ball.x += ballvelocity.x;
                    ball.y += ballvelocity.y;
                }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed (KEY_E))
                {
                    CloseWindow();
                }
                
                if (IsKeyPressed (KEY_R))
                {
                    screen = TITLE;
                }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RED);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    // TODO: Draw Logo...............................(0.2p)
                    //DrawTexture(Logo, screenWidth/2 - Logo.width/2, screenHeight/2 - Logo.height/2, WHITE);
                    
                    DrawText ("DrewaArt", screenWidth/2 - MeasureText ("DrewaArt", 50)/2, screenHeight/2 - 50, 50, Fade (recColor, alpha));
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawTexture(PantallaTitulo, 0, 0, WHITE);
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawText ("DRAGONBALL PONG", screenWidth/2 - MeasureText ("DRAGONBALL PONG", 50)/2, screenHeight/2 - 50, 50, Fade (tituloColor, alpha));
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText("PRESS ENTER TO START", screenWidth/2 - MeasureText ("PRESS ENTER TO START", 30)/2, screenHeight/2,30, WHITE);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    //ClearBackground (WHITE);
                    
                    DrawTexture(PantallaGameplay, 0, 0, WHITE);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    //pintamos pala derecha (enemy)
                    DrawRectangleRec (paladerecha, RED);
                    
                    //pintamos pala izquierda (player)
                    DrawRectangleRec (palaizquierda, YELLOW);
                    
                    //pintamos la pelota
                    DrawCircleV (ball, ballSize, ORANGE);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    //Barras de vida del jugador
                    DrawRectangleRec (LifePlayerBack, DARKGRAY);
                    
                    DrawRectangleRec (LifePlayer, GREEN);
                    
                    //Barras de vida del enemigo
                    DrawRectangleRec (LifeEnemyBack, DARKGRAY);
                    
                    DrawRectangleRec (LifeEnemy, GREEN);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%i", timeCounter), screenWidth/2 - 20, 10 , 40, DARKGRAY);
                    
                    // TODO: Draw pause message when required........(0.5p)
                   
                    if (pause){
                        DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });
                        DrawText ("PAUSE", screenWidth/2 - MeasureText ("PAUSE", 40)/2, screenHeight/2, 40, DARKPURPLE);
                        //if (blink)
                    }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    ClearBackground (PINK);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                    if (playerGoal == 10)
                    {
                        DrawTexture(Victoria, 0, 0, WHITE);
                        
                        DrawText ("YOU WON!", screenWidth/2 - MeasureText ("YOU WON!", 40)/2, screenHeight/2 - 40, 40, WHITE);
                    }
                    else if (enemyGoal == 10)
                    {
                        DrawTexture(Derrota, 0, 0, WHITE);
                        DrawText ("YOU LOOSE!", screenWidth/2 - MeasureText ("YOU LOOSE!", 40)/2, screenHeight/2 - 40, 40, PURPLE);
                    }
                    else
                    {
                        DrawText ("TIME'S UP!", screenWidth/2 - MeasureText ("TIME'S UP!", 40)/2, screenHeight/2 - 40, 40, DARKPURPLE);
                    }
                    
                    DrawText ("What do you want to do?", screenWidth/2 - MeasureText ("What do you want to do?", 30)/2, screenHeight/2 - 10, 30, RED);
                    
                    DrawText ("E - EXIT", screenWidth/4 - MeasureText ("E - EXIT", 20)/2, screenHeight/2 + 30, 20, WHITE);
                    DrawText ("R - REPLAY", 3*screenWidth/4 - MeasureText ("R - REPLAY", 20)/2, screenHeight/2 + 30, 20, WHITE);
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    //UnloadTexture(Logo);
    UnloadTexture(PantallaGameplay);
    UnloadTexture(PantallaTitulo);
    UnloadTexture(Victoria);
    UnloadTexture(Derrota);
    
    UnloadSound(fxGoal);
    UnloadSound(fxRebote);
    
    UnloadMusicStream(Gameplay);
    
    CloseAudioDevice();
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
 return 0;
 
}